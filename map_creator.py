import cv2

img = cv2.imread('test.png', cv2.IMREAD_GRAYSCALE)

print("[")
for row in img:
    print("[", end='')
    for pixel in row:
        #print(pixel)
        if pixel==255:
            print(" 1,", end='')
        elif pixel==0:
            print(" 0,", end='')
        elif pixel==85:
            print(" 7,", end='')
        else: # 170
            print("-1,", end='')
    print("],")
print("]")
