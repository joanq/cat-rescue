""" Create game structure and load all resources """

import pygame
import pygame.freetype

def create_game():
    """ Create the game and load all resources """
    game={}
    game['tiles']=load_tiles()
    game['sounds']=load_sounds()
    game['messages']=[]
    game['fonts']=load_fonts()
    return game

def load_tiles():
    """ Load all tiles used in the game (except characters) """
    tiles={}
    tiles['background']= \
        pygame.image.load("images/ice0.png"), \
        pygame.image.load("images/crystal_wall02.png"), \
        pygame.image.load("images/ice0.png"), \
        pygame.image.load("images/lava1.png"), \
        pygame.image.load("images/lava2.png"), \
        pygame.image.load("images/lava3.png"), \
        pygame.image.load("images/lava4.png"), \
        pygame.image.load("images/water1.png"), \
        pygame.image.load("images/water2.png"), \
        pygame.image.load("images/water3.png"), \
        pygame.image.load("images/water4.png"), \
        pygame.image.load("images/water5.png"), \
        pygame.image.load("images/red_door.png"), \
        pygame.image.load("images/yellow_door.png"), \
        pygame.image.load("images/blue_door.png"), \
        pygame.image.load("images/green_door.png"), \
        pygame.image.load("images/purple_door.png"), \
        pygame.image.load("images/cyan_door.png")
    tiles['obj_images']= \
        pygame.image.load("images/stairs_down.png"), \
        pygame.image.load("images/stairs_up.png"), \
        pygame.image.load("images/cat_down_w.png"), \
        pygame.image.load("images/cat_down_b.png"), \
        pygame.image.load("images/book.png"), \
        pygame.image.load("images/crystal.png"), \
        pygame.image.load("images/potion.png"), \
        pygame.image.load("images/cloud_fire_0.png"), \
        pygame.image.load("images/cloud_fire_1.png"), \
        pygame.image.load("images/cloud_fire_2.png"), \
        pygame.image.load("images/cloud_forest_fire.png"), \
        pygame.image.load("images/cloud_magic_trail_0.png"), \
        pygame.image.load("images/cloud_magic_trail_1.png"), \
        pygame.image.load("images/cloud_magic_trail_2.png"), \
        pygame.image.load("images/cloud_magic_trail_3.png"), \
        pygame.image.load("images/portal1.png"), \
        pygame.image.load("images/portal2.png"), \
        pygame.image.load("images/portal3.png"), \
        pygame.image.load("images/open_door.png"), \
        pygame.image.load("images/red_key.png"), \
        pygame.image.load("images/yellow_key.png"), \
        pygame.image.load("images/blue_key.png"), \
        pygame.image.load("images/green_key.png"), \
        pygame.image.load("images/purple_key.png"), \
        pygame.image.load("images/cyan_key.png")
    tiles['cat_images']= \
        pygame.image.load("images/cat_up_w.png"), \
        pygame.image.load("images/cat_down_w.png"), \
        pygame.image.load("images/cat_left_w.png"), \
        pygame.image.load("images/cat_right_w.png"), \
        pygame.image.load("images/cat_up_b.png"), \
        pygame.image.load("images/cat_down_b.png"), \
        pygame.image.load("images/cat_left_b.png"), \
        pygame.image.load("images/cat_right_b.png")
    tiles['inventory']=pygame.image.load("images/inventory.png")
    tiles['icons']=pygame.image.load('images/cat_icon.png'), \
        pygame.image.load('images/footsteps_icon.png'), \
        pygame.image.load('images/bindle_icon.png'), \
        pygame.image.load('images/stairs_icon.png')
    return tiles

def load_sounds():
    """ Load all sounds """
    pygame.mixer.init()
    pygame.mixer.music.set_volume(0.7)
    sounds={}
    sounds['meow_sound'] = pygame.mixer.Sound("sounds/Meow.ogg")
    return sounds

def load_fonts():
    """ Load all fonts """
    fonts={
        'main': pygame.freetype.Font("fonts/Lato-Black.ttf", 64),
        'messages': pygame.freetype.Font("fonts/Lato-Black.ttf", 24)
    }
    return fonts
