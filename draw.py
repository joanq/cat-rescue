""" Draw all elements """
import pygame
import pygame.freetype

def draw(screen, game):
    """ Called every frame to draw all elements at the screen """
    screen.fill((128,128,128))
    tiles=game['tiles']
    level=game['level']
    old_man=game['old_man']
    draw_background(screen, level, tiles)
    for char in game['characters']:
        if char['alive']:
            draw_character(screen, char, level['map'])
    for char in level['monsters']:
        if char['alive']:
            draw_character(screen, char, level['map'])
    draw_portal(screen, level)
    draw_stats(screen, game)
    draw_inventory(screen, old_man, tiles)
    draw_messages(screen, game['messages'])

def draw_background(screen, level, tiles):
    """ Dibuixa el fons """
    lmap=level["map"]
    background=tiles['background']
    objects=level['objects']
    obj_images=tiles['obj_images']
    offset_x=(pygame.display.get_surface().get_width() - len(lmap[0])*32)/2
    for n_row, row in enumerate(lmap):
        for n_col, cell in enumerate(row):
            if cell>=0:
                sprite=background[cell]
                square=sprite.get_rect().move(n_col*32+offset_x, n_row*32)
                screen.blit(sprite, square)
    for n_row, row in enumerate(objects):
        for n_col, cell in enumerate(row):
            if cell!=0:
                sprite=obj_images[cell-1]
                square=sprite.get_rect().move(n_col*32+offset_x, n_row*32)
                screen.blit(sprite, square)

def draw_portal(screen, level):
    """ Animal through portal animation """
    if 'portal' in level and level['portal']['character'] is not None:
        lmap=level["map"]
        portal=level['portal']
        character=portal['character']
        sprite=character['sprite']
        frames=portal['frames']
        x=character['x']
        y=character['y']
        offset_x=(pygame.display.get_surface().get_width() - len(lmap[0])*32)/2
        sprite=pygame.transform.rotozoom(sprite, (10-frames)*10, frames/10)
        square=sprite.get_rect().move(x*32+offset_x, y*32)
        screen.blit(sprite, square)
        portal['frames']-=1
        if portal['frames']<=0:
            portal['character']=None

def draw_character(screen, character, lmap):
    """ Dibuixa un personatge """
    sprite=character["sprite"]
    x=character["x"]
    y=character["y"]
    offset_x=(pygame.display.get_surface().get_width() - len(lmap[0])*32)/2
    square=sprite.get_rect().move(x*32+character["deltax"]+offset_x, y*32+character["deltay"])
    screen.blit(sprite, square)

def draw_inventory(screen, character, tiles):
    """ Dibuixa l'inventari """
    inventory_img=tiles["inventory"]
    obj_images=tiles["obj_images"]
    icon=tiles['icons'][2]
    inventory=character["inventory"]
    x=0
    icon_square=icon.get_rect().move(550, 500)
    screen.blit(icon, icon_square)
    for x in range(0, 5):
        square=inventory_img.get_rect().move(630+x*32, 520)
        screen.blit(inventory_img, square)
        if x<len(inventory):
            obj=inventory[x]
            sprite=obj_images[obj-1]
            square=sprite.get_rect().move(630+x*32, 520)
            screen.blit(sprite, square)

def draw_stats(screen, game):
    """ Dibuixa quantitat de passos i gats """
    level=game['level']
    stairs_icon=game['tiles']['icons'][3]
    stairs_rect=stairs_icon.get_rect().move(10, 500)
    screen.blit(stairs_icon, stairs_rect)
    cat_icon=game['tiles']['icons'][0]
    cat_rect=cat_icon.get_rect().move(190, 500)
    screen.blit(cat_icon, cat_rect)
    footsteps_icon=game['tiles']['icons'][1]
    footsteps_rect=footsteps_icon.get_rect().move(370, 500)
    screen.blit(footsteps_icon, footsteps_rect)
    game['fonts']['main'].render_to(screen, (90, 510), \
        f"{game['current_level']}", (87, 218, 230))
    game['fonts']['main'].render_to(screen, (270, 510), \
        f"{level['n_cats']}", (87, 218, 230))
    game['fonts']['main'].render_to(screen, (450, 510), \
        f"{level['n_steps']}", (87, 218, 230))
    if game['over']:
        if game['win']:
            text, rect=game['fonts']['main'].render('YOU WIN!', (70,97,99))
            offset_x=(pygame.display.get_surface().get_width()-rect.width)/2
            screen.blit(text, (offset_x,200))
        else:
            text, rect=game['fonts']['main'].render('GAME OVER!', (70,97,99))
            offset_x=(pygame.display.get_surface().get_width()-rect.width)/2
            screen.blit(text, (offset_x,200))

def draw_messages(screen, messages):
    """ Show a message to the user """
    if len(messages)>0:
        msg=messages[0]
        if msg['time']<=0:
            messages.pop(0)
        else:
            text,rect=msg['font'].render(msg['text'], msg['color'])
            surface = pygame.Surface((rect.width+20, rect.height+20))
            surface.fill((0,0,0))
            surface.blit(text, (10,10))
            offset_x=(pygame.display.get_surface().get_width()-rect.width)/2
            screen.blit(surface, (offset_x,200))
            msg['time']-=1
