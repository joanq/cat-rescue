"""
Joc
"""
import pygame
import pygame.freetype
from levels import init_levels
from game import create_game
from draw import draw

def update_map(level):
    """ Animacions del mapa """
    lmap=level["map"]
    objects=level['objects']
    for row in range(0, len(lmap)):
        for col in range(0, len(lmap[0])):
            # Lava animation
            if lmap[row][col]>=3 and lmap[row][col]<=6:
                lmap[row][col] = 3+(lmap[row][col]-3+1)%4
            # Water animation
            if lmap[row][col]>=7 and lmap[row][col]<=11:
                lmap[row][col] = 7+(lmap[row][col]-7+1)%4
            # Fire animation
            if objects[row][col]>=8 and objects[row][col]<=10:
                objects[row][col] += 1
            elif objects[row][col]==11:
                objects[row][col]=0
            # Magic animation
            if objects[row][col]>=12 and objects[row][col]<=14:
                objects[row][col] += 1
            elif objects[row][col]==15:
                objects[row][col]=0
            # Portal animation
            if objects[row][col]>=16 and objects[row][col]<=18:
                objects[row][col] = 16+(objects[row][col]-16+1)%3

def die_on_move(game, character):
    """ Comprova si un personatge mor al moure's """
    lmap=game['level']['map']
    objects=game['level']['objects']
    characters=game['characters']
    x=character['x']
    y=character['y']
    if lmap[y][x]>=2 and lmap[y][x]<=5: # lava
        character["alive"]=False
        game['over']=True
        objects[y][x]=8
    if character in game['level']['monsters']:
        for char in characters:
            if x==char['x'] and y==char['y'] and char['alive']:
                char['alive']=False
                game['over']=True
                objects[y][x]=12
    else:
        for monster in game['level']['monsters']:
            if monster['x']==x and monster['y']==y and character['alive']:
                character['alive']=False
                game['over']=True
                objects[y][x]=12

def move(game, character, dx, dy):
    """ Mou, si pot, un personatge """
    moved=False
    lmap=game['level']['map']
    if dx>0:
        character["sprite"]=character["right"]
    if dx<0:
        character["sprite"]=character["left"]
    if dy>0:
        character["sprite"]=character["down"]
    if dy<0:
        character["sprite"]=character["up"]
    new_x=character["x"]+dx
    new_y=character["y"]+dy
    cell=lmap[new_y][new_x]
    # Can move if:
    # - Normal ground
    # - Lava (at your own risk)
    # - Water only if not coming with pets
    if cell<1 or (cell>=3 and cell<=6) or \
            (cell>=7 and cell<=11 and 'pets' in character and len(character['pets'])==0):
        character["x"]=new_x
        character["y"]=new_y
        moved=True
        die_on_move(game, character)
    return moved

def take_object(character, level):
    """ Personatge agafa un objecte """
    taken=False
    objects=level["objects"]
    x=character["x"]
    y=character["y"]
    obj=objects[y][x]
    if obj>=20 and len(character["inventory"])<5:
        objects[y][x]=0
        character["inventory"].append(obj)
        taken=True
    return taken

def drop_object(character, index, level):
    """ Personatge deixa un objecte """
    dropped=False
    objects=level["objects"]
    x=character["x"]
    y=character["y"]
    inventory=character["inventory"]
    floor=objects[y][x]
    if floor==0 and len(inventory)>index:
        objects[y][x]=inventory.pop(index)
        dropped=True
    return dropped

def use_object(character, index, level):
    """ Character use an object """
    lmap=level["map"]
    objects=level["objects"]
    x=character["x"]
    y=character["y"]
    inventory=character["inventory"]
    if len(inventory)>index:
        obj = inventory[index]
        if obj>=20 and obj<=25: # key
            dx=0
            dy=0
            if character["sprite"]==character["right"]:
                dx=1
            elif character["sprite"]==character["left"]:
                dx=-1
            elif character["sprite"]==character["up"]:
                dy=-1
            elif character["sprite"]==character["down"]:
                dy=1
            tile=lmap[y+dy][x+dx]
            if tile+8==obj:
                inventory.pop(index)
                lmap[y+dy][x+dx]=0
                objects[y+dy][x+dx]=19

def move_player(game, dx, dy):
    """ Mou el personatge principal """
    moved = move(game, game['old_man'], dx, dy)
    if moved:
        pets=game['old_man']['pets']
        for n_pet in range(len(pets)-1, -1, -1):
            pet=pets[n_pet]
            if move_pet(game, pet):
                pets.pop(n_pet)
                game['characters'].remove(pet)
            pet["path"].append((dx, dy))
        move_monsters(game)
        objects=game['level']['objects']
        x=game['old_man']['x']
        y=game['old_man']['y']
        if objects[y][x]==1 and game['level']['n_cats']==0: # stairs down
            game['current_level']+=1
            if game['current_level'] < len(game['levels']):
                game['level']=game['levels'][game['current_level']]
                start_level(game)
            else:
                game['over']=True
                game['win']=True
        else:
            game['level']['n_steps']-=1
            if game['level']['n_steps']<=0:
                game['over']=True
        #if objects[y][x]==2: # stairs up
        #    game['current_level']-=1
        #    game['level']=game['levels'][game['current_level']]
        #    start_level(game)
        if objects[y][x]==3: # cat
            objects[y][x]=0
            cat = create_cat(game['old_man'], 'w', game['tiles']['cat_images'])
            game['characters'].insert(0, cat)
            game['level']['n_cats']-=1
            pygame.mixer.Sound.play(game['sounds']['meow_sound'])
        if objects[y][x]==4: # cat
            objects[y][x]=0
            cat = create_cat(game['old_man'], 'b', game['tiles']['cat_images'])
            game['characters'].insert(0, cat)
            game['level']['n_cats']-=1
            pygame.mixer.Sound.play(game['sounds']['meow_sound'])
        # Remove dead characters
        characters=game['characters']
        n_char=len(game['characters'])-1
        for n_ch in range(n_char, -1, -1):
            if not characters[n_ch]['alive']:
                characters.pop(n_ch)
        monsters=game['level']['monsters']
        n_monsters=len(monsters)-1
        for n_monster in range(n_monsters, -1, -1):
            if not monsters[n_monster]['alive']:
                monsters.pop(n_monster)
        # Show messages
        show_messages(x, y, game['messages'], game['level']['messages'])
    return moved

def show_messages(x, y, active_messages, level_messages):
    """ Show all position messages """
    for msg in level_messages:
        if msg['x']==x and msg['y']==y:
            active_messages.append(msg)

def move_monsters(game):
    """ Move all monsters """
    for monster in game['level']['monsters']:
        if monster['alive']:
            move_monster(game, monster)

def move_monster(game, monster):
    """ Move a single monster """
    movement=monster["path"][monster["pos_path"]]
    monster["pos_path"]=(monster["pos_path"]+1)%len(monster["path"])
    return move(game, monster, movement[0], movement[1])

def move_pet(game, pet):
    """ Move a pet. Returns if the pet gets free """
    free=False
    if len(pet["path"])>=pet['pos']:
        movement=pet["path"].pop(0)
        moved=move(game, pet, movement[0], movement[1])
        if moved:
            objects = game['level']['objects']
            x=pet['x']
            y=pet['y']
            if objects[y][x]>=16 and objects[y][x]<=18: # portal
                if 'portal' in game['level']:
                    game['level']['portal']['character']=pet
                    game['level']['portal']['frames']=10
                free=True
    return free

def start_level(game):
    """ Start a new level """
    old_man=game['old_man']
    level=game['level']
    old_man['x']=level['start'][0]
    old_man['y']=level['start'][1]
    show_messages(old_man['x'], old_man['y'], game['messages'], level['messages'])
    # Rearrange pets at the start of the level
    pets=old_man['pets']
    n_pets=len(pets)-1
    for n_pet in range(n_pets, -1, -1):
        if not pets[n_pet]['alive']:
            pets.pop(n_pet)
    for pos, pet in enumerate(pets):
        pet['x']=old_man['x']
        pet['y']=old_man['y']
        pet['path']=[]
        pet['pos']=2+pos

def create_monster():
    """ Create a monster """
    image = pygame.image.load("images/monster.png")
    image_inv = pygame.transform.flip(image, True, False)
    monster={
        "up": image,
        "down": image_inv,
        "left": image,
        "right": image_inv,
        "deltax": 0,
        "deltay": 0,
        "pets": [],
        "alive": True,
        "pos_path": 0
    }
    monster["sprite"]=monster["down"]
    return monster

def create_old_man():
    """ Create the main character """
    old_man={
        "up": pygame.image.load("images/up.png"),
        "down": pygame.image.load("images/down.png"),
        "left": pygame.image.load("images/left.png"),
        "right": pygame.image.load("images/right.png"),
        "x": 1,
        "y": 3,
        "deltax": 8,
        "deltay": -7,
        "pets": [],
        "inventory": [],
        "alive": True
    }
    old_man["sprite"]=old_man["down"]
    return old_man

def create_cat(character, color, cat_images):
    """ Create a cat """
    index=0
    if color=='b':
        index+=4
    cat={
        "up": cat_images[index],
        "down": cat_images[index+1],
        "left": cat_images[index+2],
        "right": cat_images[index+3],
        "x": character['x'],
        "y": character['y'],
        "deltax": 0,
        "deltay": 0,
        "path": [],
        "pos": 2+len(character['pets']),
        "alive": True
    }
    cat["sprite"]=cat["down"]
    character['pets'].append(cat)
    return cat

def key_down(event, game):
    """ Called when the user hits a key in-game """
    old_man=game['old_man']
    level=game['level']
    if event.key == pygame.K_RIGHT or event.key == pygame.K_d:
        move_player(game,1,0)
    if event.key == pygame.K_LEFT or event.key == pygame.K_a:
        move_player(game,-1,0)
    if event.key == pygame.K_UP or event.key == pygame.K_w:
        move_player(game,0,-1)
    if event.key == pygame.K_DOWN or event.key == pygame.K_s:
        move_player(game,0,1)
    if event.key == pygame.K_SPACE:
        take_object(old_man,level)
    if event.key == pygame.K_z:
        drop_object(old_man,0,level)
    if event.key == pygame.K_x:
        drop_object(old_man,1,level)
    if event.key == pygame.K_c:
        drop_object(old_man,2,level)
    if event.key == pygame.K_v:
        drop_object(old_man,3,level)
    if event.key == pygame.K_b:
        drop_object(old_man,4,level)
    if event.key == pygame.K_1:
        use_object(old_man,0,level)
    if event.key == pygame.K_2:
        use_object(old_man,1,level)
    if event.key == pygame.K_3:
        use_object(old_man,2,level)
    if event.key == pygame.K_4:
        use_object(old_man,3,level)
    if event.key == pygame.K_5:
        use_object(old_man,4,level)

def init_game(game):
    """ Init the game at the beginning or after dying """
    game['old_man']=create_old_man()
    game['current_level']=0
    game['levels']=init_levels(game['fonts']['messages'])
    game['level']=game['levels'][game['current_level']]
    game['characters']=[game['old_man']]
    game['over']=False
    game['win']=False

def main():
    """ Main function, the beginning of all """
    pygame.init()
    game=create_game()
    init_game(game)
    start_level(game)

    screen = pygame.display.set_mode([800, 600])
    pygame.display.set_caption('Kitten Rescue')
    pygame.display.set_icon(game['tiles']['icons'][0])
    clock = pygame.time.Clock()
    frames=0
    going=True
    while going:
        clock.tick(40)
        frames=(frames+1)%10
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
            if not game['over']:
                if event.type == pygame.KEYDOWN:
                    key_down(event, game)
            else:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        init_game(game)
                        start_level(game)
        if frames==0:
            update_map(game['level'])
        draw(screen, game)
        pygame.display.flip()
    pygame.quit()

main()
